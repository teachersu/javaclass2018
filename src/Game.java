public class Game {
	String controls;
	String color;
	String[] charactors;
	String[] settings;
	int score;

	public void play() {
		System.out.println("Let's play");
	}

	public void pause() {
		System.out.println("Wait, I need to go to restroom.");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Let's continue");
	}

	public void increaseScore() {
		score = score + 1;
	}

	public void saveScore(String player) {
		// save to somepalce.
		System.out.println("Score " + score + " is saved for player " + player);
	}

}